// @Bonsai Lab - 2018
// A project in Politecnico di Milano
// Author: Nguyen Van Minh nguyenvanminh.vnu@gmail.com
// Description:
// 		- NetworkMonitor service defined for server
// 		- When receiving proper request command from stub, server invokes shell script over child process and write back output log to stub
// 		- Currently permit 2 type of shell command:
//			+ ping
//			+ iperf (iperf2)

'use strict';
const grpc = require('grpc');
const hello_proto = grpc.load('./proto/network_monitor.proto');
const { spawn } = require( 'child_process' );

function networkMonitor(call) {
	call.on('data', function(mes){
		console.log('Client: ', mes.message);
		

		if (mes.message.includes('iperf') || mes.message.includes('ping')) {
			call.write({message: 'Running this command: ' + mes.message});
			let cmd = spawn(mes.message, {shell: true});
			cmd.stdout.on('data', data => {
				call.write({message: data.toString()});
			})
		}
	});

	call.on('end', function() {
		console.log('End');
    call.end();
  });

  call.write({message: 'Hi'});
}

// function monitorNic(call) {
// 	call.on('data', function(mes){
// 		if (mes.message == 'Bandwidth') {
// 			// var cmd = 'typeperf -cf testShell.cfg -sc 10';
// 			var cmd = 'ifstat -a -t';
// 			// shell.exec(cmd, function(code, stdout, stderr){
// 			// 	console.log('Code ',code);
// 			// 	console.log('Stdout ', stdout);
// 			// 	console.log('Stderr ',stderr);
// 			// 	// call.write(stdout);
// 			// 	call.write(code);
// 			// })
// 			// rl.on('line', function(input) {
// 			// 	if (input == 'end') {
// 			// 		child.kill('SIGINT');
// 			// 	}
// 			// })
// 			var child = shell.exec(cmd, {async: true});
// 			child.stdout.on('data', function(data){
// 				call.write({message: data});
// 				// if (mes.message == 'end') {
// 				// 	console.log('Stop');
// 				// 	shell.exit(1);
// 				// }
// 			});

// 			// rl.on('line', function(input) {
// 			// 	console.log(input);
// 			// 	if (input == 'end') {
// 			// 		child.kill('SIGINT');
// 			// 		// process.exit(0);
// 			// 		// child.exit();
// 			// 		// shell.exit();
// 			// 	}
// 			// })
// 		} else if (mes.message == 'end') {
// 			call.write({message: 'end'});
// 			call.end();
// 		}
// 	});

// 	call.on('end', function(){
// 		console.log('End');
// 		call.end();
// 	});

// 	call.write({message: 'server'});

// }
/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */
function main() {
  var server = new grpc.Server();
  server.addService(hello_proto.Greeter.service, {
  	networkMonitor: networkMonitor
  	// monitorNic: monitorNic
  });
  server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  server.start();
}

main();
