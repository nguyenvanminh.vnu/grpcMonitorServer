// var shell = require('shelljs');
// var grpc = require('grpc');

// shell.echo('Hello world');

// // var cmd = 'typeperf "\Network Interface(*)\Bytes Received/sec" -sc 10'
// var cmd = 'typeperf -cf testShell.cfg -sc 10';
// shell.exec(cmd, function(code, stdout, stderr){
// 	console.log(code);
// 	console.log(stdout);
// 	console.log(stderr);
// })
'use strict';
// const { Readable } = require('stream'); 
// const inStream = new Readable();
// inStream.push('ABCDEFGHIJKLM');
// setTimeout(function(){
// 	inStream.push('NOPQRSTUVWXYZ');  
// }, 4000);
// // inStream.push('NOPQRSTUVWXYZ');
// inStream.push(null); // No more data
// inStream.pipe(process.stdout);
const cp = require('child_process');
const stream = require('stream');
const fs = require('fs');
// use a Writable stream
// var customStream = new stream.Writable();
// customStream._write = function (data) {
//     console.log(data.toString());
// };

// var s = fs.openSync('test.txt', 'w');
// // var p = cp.spawn('iperf3 -c 10.79.1.58 -t 10 -i 2', [], {shell: true, stdio: [process.stdin, s, process.stderr]});
// var p = cp.spawn('ifstat -a -t', [], {shell: true, stdio: [process.stdin, s, process.stderr]});
// 'pipe' option will keep the original cp.stdout
// 'inherit' will use the parent process stdio
// var child = cp.spawn('iperf3 -c 10.79.1.58 -t 10 -i 2', {shell: true });
// var child = cp.spawn('ifstat -a -t', {shell: true });
var child = cp.spawn('iperf -c 10.79.1.58 -t 20 -i 2', {shell: true });

child.stdout.on('data', data => {
	console.log('stdout' + data.toString());
})
// process.stdin.on('readable', () => {
//   const chunk = process.stdin.read();
//   if (chunk !== null) {
//     process.stdout.write(`data: ${chunk}`);
//   }
// });
// process.stdout.on('data', function (chunk) {
//     console.log(chunk);
// });

// pipe to your stream
// child.stdout.pipe(customStream);
// var spawnedProcess = cp.spawn('iperf3 -c 10.79.1.58 -t 10 -i 2', {shell: true});
// spawnedProcess.stdout.pipe(writable);

// const { spawn } = require( 'child_process' ), 
// // ls = spawn( 'iperf3', [ '-c', '10.79.1.58', '-t', '10', '-i', '2' ], {detached: true, stdout: ['ignore', 1, 2]});
// ls = spawn('iperf3 -c 10.79.1.58 -t 10 -i 2', {shell: true, stdio: 'inherit'});

// ls.unref();
// ls.stdout.pipe(process.stdout);
// ls.stdout.on( 'data', data => {
//     // console.log( `stdout: ${data}` );
//     process.stdout.write(data.toString());
//     // console.log('stdout: ' + data.toString());
// } );

// ls.stderr.on( 'data', data => {
//     console.log( `stderr: ${data}` );
// } );

// ls.on( 'close', code => {
//     console.log( `child process exited with code ${code}` );
// } );


// 'use strict';

// const
//     { spawnSync } = require( 'child_process' ),
//     ls = spawnSync( 'iperf3', [ '-c', '10.79.1.58', '-t', '20', '-i', '2' ] );

// // console.log( `stderr: ${ls.stderr.toString()}` );
// console.log( `stdout: ${ls.stdout.toString()}` );